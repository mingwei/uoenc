#include "uoheader.h"


/*
	hmacPrep
		GOAL: Prepare hmac digest handle.
		RETURN: 0-success, 1-unsuccess.
*/
int hmacPrep(gcry_md_hd_t* d, char* key)
{
	gcry_error_t gcryError = 0;

	gcryError = gcry_md_open(d, GCRY_MD, GCRY_MD_FLAG_SECURE|GCRY_MD_FLAG_HMAC);
	if(gcryError)
	{
		printf("gcry_md_open failed: %s/%s\n",gcry_strsource(gcryError),gcry_strerror(gcryError));
		return 1;
	}

	gcryError = gcry_md_setkey(*d,key,strlen(key)); 
	if(gcryError)
	{
		printf("gcry_md_setkey failed: %s/%s\n",gcry_strsource(gcryError),gcry_strerror(gcryError));
		return 1;
	}

	return 0;	
}


/*
	cipherPrep
		GOAL: Prepare cipher handle.
		RETURN: 0-success, 1-unsuccess.
*/
int cipherPrep(gcry_cipher_hd_t* handle, char* iv, char* key)
{
	gcry_error_t	gcryError;

	/* CIPHER OPEN */
	gcryError = gcry_cipher_open(handle,GCRY_CIPHER,GCRY_C_MODE,0);
	if(gcryError)
	{
		printf("gcry_cipher_open failed: %s/%s\n",gcry_strsource(gcryError),gcry_strerror(gcryError));
	}
	
	/* CIPHER SETKEY */
	gcryError = gcry_cipher_setkey(*handle,key,32);
	if(gcryError)
	{
		printf("gcry_cipher_setkey failed: %s/%s\n",gcry_strsource(gcryError),gcry_strerror(gcryError));
		return 1;
	}

	/* CIPHER SETIV */
	gcryError = gcry_cipher_setiv(*handle,iv,GCRY_IV_SIZE);
	if(gcryError)
	{
		printf("gcry_cipher_setiv failed: %s/%s\n",gcry_strsource(gcryError),gcry_strerror(gcryError));
		return 1;
	}

	return 0;
}

/*
	genKey
		GOAL: generate key by gcry_kdf_derive function.
		RETURN: buffer as key.
*/
char* genKey(char* password, char* salt)
{
	char * key = (char*)calloc(32,sizeof(char));
	/* GENERATE KEY */
	int gcryError = gcry_kdf_derive(password, 8, GCRY_KDF_PBKDF2, GCRY_CIPHER_AES256, salt, SALT_SIZE, 10, 32, key) ;
	if(gcryError)
	{
		printf("gcry_kdf_derive failed: %s/%s\n",gcry_strsource(gcryError),gcry_strerror(gcryError));
		return NULL;
	}

	return key;
}

/*
	getRndBuf
		GOAL: generate random bytes filled buffer, used as IV or SALT
*/
void getRndBuf(char* buf, int ksize)
{
	int i;
	srandom(time(NULL));
	for(i=0;i<ksize;i++)
	{
		buf[i]=random();
	}
}

/*
	options
		GOAL: display options.
*/
void options()
{
	printf("OPTIONS: -d <IP-addr:port>  [-l] (local)\n");
}

/*
	error
		GOAL: display error messages.
*/
void error(const char *msg)
{
    perror(msg);
}

/*
	getPassword
		GOAL: prompt for password.
*/
char * getPassword()
{
	char * passwd = (char*)calloc(100,sizeof(char));
	printf("Password: ");
	gets(passwd);

	return passwd;
}

/*
	print_hex
		GOAL: DEBUG -> show hex bytes.
*/
void print_hex(unsigned char *buf, int len)
{
  int i;
  int n;

  puts("-------- HEX DEBUG --------");
  for(i=0,n=0;i<len;i++){
    printf("%02x",buf[i]);
  }
  printf("\n");
}

/*
	testFileExists
		GOAL: test if file exists.
		RETURN: 1-if file exists; 0-if file does not exists
*/
int testFileExists(char * fname)
{
	FILE *file;
	if ((file = fopen(fname, "r")) == NULL) 
	{
		return 0;
	} 
	else 
	{
	    fclose(file);
	}
	return 1;
}
