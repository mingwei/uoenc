#include "uoheader.h"

int isLocal=0;
int isRemote=0;

gcry_md_hd_t 		*digest;
gcry_cipher_hd_t 	*handle;
char* iv;

/*
	main(int argc, char **argv)
		main entrance. process options using getopt.

*/
int main (int argc, char **argv)
{
	char * sock_str = NULL;
	char * inputFile = NULL;
	char outputFile[100];
	int c;				// option value


	if(argc<2)
	{
		fprintf(stderr, "Option <input file> is required as an argument.\n");
		return 1;
	}


	while(( c = getopt (argc, argv, "ld:")) !=-1)	// getopt -- ":" means argument needed.
		switch(c)
			{
			case 'l':
				isLocal = 1;
				break;
			case 'd':
				sock_str = optarg;
				isRemote = 1;
				break;
			default:
				options();
			}
			
	inputFile = argv[optind];
	if(inputFile==NULL)
	{
		printf("ERROR: NEED FILE NAME\n");
		exit(1);
	}

	sprintf(outputFile, "%s.uo",inputFile);		// add suffix "uo" to output file name.

	if(isLocal == 1)
	{
		int res = localEnc(inputFile,outputFile);	
	}
	
	if(isRemote == 1)
	{
		
		int res = localEnc(inputFile,outputFile);	
		if(res==0)
			startClient(outputFile,sock_str);
	}

}


/*
	localEnc
		GOAL: Perform encryption on local file. Encrypted file is written locally.
		RETURN: 0-success, 1-unsuccess
*/
int localEnc(char * inputFile, char * outputFile)
{
	FILE* in = NULL;
	FILE* out = NULL;
	long lSize;
	int bufsize=1024;
	char * buf = (char*)malloc((sizeof(char))*(bufsize)); 
	char * outbuf=(char*)malloc((sizeof(char))*(bufsize)); 
	unsigned char * hmacBuf;

	// assign memory space for cipher and digest handles
	handle = (gcry_cipher_hd_t*) malloc(sizeof(gcry_cipher_hd_t)); 
	digest = (gcry_md_hd_t*) malloc(sizeof(gcry_md_hd_t)); 


	printf("---- LOCAL ----\n");

	// Open input file as read mode.
    in = fopen(inputFile,"rb");
    if(NULL == in)
    {
        printf("\n fopen() Error!!!\n");
        return 1;
    }

	// Open output file as write mode.
	// if output file already existed, then return with error code.
	if(testFileExists(outputFile)>0)
	{
		puts("File Exists!");
		return 1;
	}
	out = fopen(outputFile,"w");
    if(NULL == out)
    {
        printf("\n fopen() Error!!!\n");
        return 1;
    }


	// Obtain file size:
	fseek (in , 0 , SEEK_END);
	lSize = ftell (in);
	rewind (in);
	
	// Prepare encryption and hmac handles. Generate secret key.
	char * salt= (char*) malloc(SALT_SIZE*sizeof(char));
	getRndBuf(salt,SALT_SIZE);
	char * passwd=getPassword();
	char * key = genKey(passwd,salt);
	print_hex(key,sizeof(key));
	iv = (char*)calloc(GCRY_IV_SIZE,sizeof(char));
	getRndBuf(iv,GCRY_IV_SIZE);		// generate random buffer as IV
	cipherPrep(handle,iv,key);
	hmacPrep(digest, key);
	
	// Read loop, do encryption and digest.
	int num = fread(buf,1,bufsize,in);
	int wrote,total=0;
	while(num!=0)
	{
		printf("read %d bytes, ",num);
		gcry_md_write(*digest,buf,num);
		encrypt(outbuf,buf,num,key);	
		wrote = fwrite(outbuf,1,num,out);
		total+=wrote;
		printf("wrote %d bytes\n",wrote);
		num = fread(buf,1,bufsize,in);
	}

	// Write salt to the encrypted file.
	wrote = fwrite(salt, 1, SALT_SIZE, out);
	total+=wrote;
	printf("wrote %d bytes (salt)\n",wrote);

	// Write IV to the encrypted file.
	wrote = fwrite(iv, 1, GCRY_IV_SIZE, out);
	total+=wrote;
	printf("wrote %d bytes (iv)\n",wrote);

	// Finalize digest(hmac), append to the end of file.
	hmacBuf = gcry_md_read(*digest,GCRY_MD);
	wrote = fwrite(hmacBuf,1,KEYLEN_SHA1,out);
	printf("wrote %d bytes (hmac)\n",wrote);
	total+=wrote;
	printf("Successfully encrypted %s to %s (%d bytes written)\n",inputFile,outputFile,total);
	
	// Close handles 
	gcry_md_close(*digest);
	gcry_cipher_close(*handle);

	// Free memory
	free(digest);
	free(handle);
	free(buf);
	free(outbuf);
	free(iv);
	free(salt);
	free(key);
	free(passwd);

	fclose(out);
	fclose(in);
	return 0;
}


/*
	startClient
		Connect to decryption daemon. First transfer file. Then wait for the decryption result:
			success, file exists, open input failed, open output failed.
		RETURN: 0-success, 1-file open failed, 2- connection failed.
*/
int startClient(char* encFile,char* sockstr)
{
	int bufsize=1024;
	char * buf = (char*)malloc((sizeof(char))*(bufsize)); 
	int bytes_sent, lSize;
	
	// Parse IP and Port number from argument.
	char *ip,*port,*saveptr;
	ip = strtok(sockstr,":");
	port = strtok(NULL,":");

	printf("---- NETWORK ----\n");
	printf("Transmitting to %s:%s\n",ip,port);
	
	// First, load up address structs with getaddrinfo():
	struct addrinfo hints, *res;
	int sockfd,totalSent=0;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	int s = getaddrinfo(ip, port, &hints, &res);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
    }
	
	// Make a socket:
	sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if(connect(sockfd, res->ai_addr, res->ai_addrlen) <0)
	{
		error("ERROR: Connection Failed ");
		return 2;
	}

	// Open the file to be encrypted.
	FILE *in = fopen(encFile,"rb");
    if(NULL == in)
    {
        printf("\n fopen() Error!!!\n");
        return 1;
    }

	// Get file size
	fseek (in , 0 , SEEK_END);
	lSize = ftell (in);
	rewind (in);

	// Sent file size and file name.
	bytes_sent = send(sockfd, &lSize, sizeof(int) , 0);
	printf("size: %d bytes\n",bytes_sent);
	int namesize=strlen(encFile);
	bytes_sent = send(sockfd, &namesize, sizeof(int),0);	
	bytes_sent = send(sockfd, encFile, strlen(encFile),0);	
	printf("name: %d bytes\n",bytes_sent);

	int num = fread(buf,1,bufsize,in);
	while(num!=0)
	{
		bytes_sent = send(sockfd, buf, num , 0);
		totalSent += bytes_sent;
		num = fread(buf,1,bufsize,in);
	}

	fclose(in);

	if(totalSent==lSize)
	{
		puts("Successfully sent.");
	}
	else
	{
		puts("ERROR: Sent less bytes!");
	}

	// wait for decryption daemon to reply.
	// the reply is the result or error code for decryption.
	int decRet=0;
	recv(sockfd,&decRet,sizeof(int),0);
	if(decRet==0)
		puts("Successfully decrypted.");
	else if(decRet==2)
		puts("ERROR REMOTE: Target File Exists.");
	else if (decRet==3)
		puts("ERROR REMOTE: Input File Open Error.");
	else if (decRet==4)
		puts("ERROR REMOTE: Output File Open Error.");
	else
		puts("ERROR REMOTE: Decryption Error.");
}

/*
	encrypt
		Take buf as input, encrypt by handle with key, and then push to outbuf.
		RETURN: 0-success, 1-encryption error.
*/
int encrypt(char* outbuf,char * buf,int size,char * key)
{
	/* CIPHER ENCRYPT */
	int gcryError = gcry_cipher_encrypt(*handle,outbuf, 1024, buf, size);
	if(gcryError)
	{
		printf("gcry_cipher_encrypt failed: %s/%s\n",gcry_strsource(gcryError),gcry_strerror(gcryError));
		return 1;
	}
	return 0;
}



