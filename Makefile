all: enc dec

enc:
	gcc -g -o uoenc uoenc.c uocommon.c uoheader.h -lgcrypt 

dec:
	gcc -g -o uodec uodec.c uocommon.c uoheader.h -lgcrypt 

clean:
	rm -f uoenc uodec *.gch
