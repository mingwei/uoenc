#include "uoheader.h"

int isLocal=0;
int isRemote=0;

gcry_md_hd_t 		*digest;
gcry_cipher_hd_t 	*handle;


/*
	main(int argc, char **argv)
		main entrance. process options using getopt.

*/
int main (int argc, char **argv)
{
	char * sock_str = (char*)malloc(100*sizeof(char));
	char * inputFile = (char*)malloc(100*sizeof(char));
	int c;				// option value


	while(( c = getopt (argc, argv, "ld:")) !=-1)
		switch(c)
			{
			case 'l':
				isLocal = 1;
				break;
			case 'd':
				if(optarg == NULL)
				{
					printf("NULL OPT!\n");
					exit(1);
				}
				sock_str = strdup(optarg);
				isRemote = 1;
				break;
			default:
				options();
			}
			
	if(isLocal == 1)
	{
		inputFile = argv[optind];
		if(inputFile==NULL)
		{
			printf("NEED FILE NAME\n");
			exit(1);
		}
		int res = localDec(inputFile,0);	
	}
	
	if(isRemote == 1)
	{
		startServer(sock_str); 
	}

}

/*
	localDec
		GOAL: Perform decryption on local file. Encrypted file is written locally.
		RETURN: 0-success, 1-decryption error, 2-output file exists, 3-input file open error
				4-output file open error.
*/
int localDec(char * inputFile,int mode)
{
	FILE* in = NULL;
	FILE* out = NULL;
	long lSize;
	int bufsize=1024, totalRead=0, ret=0;
	char * buf = (char*)malloc((sizeof(char))*(bufsize)); 
	char * outbuf=(char*)malloc((sizeof(char))*(bufsize)); 
	char * outFile=(char*)malloc((sizeof(char))*(bufsize)); 
	unsigned char * hmacBuf=(char*)malloc(sizeof(char)*KEYLEN_SHA1);
	unsigned char * hmacOrg=(char*)malloc(sizeof(char)*KEYLEN_SHA1);
	char *iv=(char*)calloc(GCRY_IV_SIZE,(sizeof(char))); 
	char *salt=(char*)calloc(SALT_SIZE,(sizeof(char))); 

	handle = (gcry_cipher_hd_t*) malloc(sizeof(gcry_cipher_hd_t)); 
	digest = (gcry_md_hd_t*) malloc(sizeof(gcry_md_hd_t)); 

	puts("---- LOCAL ----");

	// 2 modes: 
	// 		a. receiving file from client
	//		b. local stored file.
	if(mode==1)
	{
		sscanf(inputFile,"/tmp/%s.uo",outFile);
		char * suffix = strrchr(outFile,'.');
		if(suffix!=NULL)
			suffix[0]='\0';
	}
	else
	{
		sscanf(inputFile,"%s.uo",outFile);
		char * suffix = strrchr(outFile,'.');
		if(suffix!=NULL)
			suffix[0]='\0';
	}

	// Open input file as read mode.
	in = fopen(inputFile,"rb");
	if(NULL == in)
	{
	   printf("\n fopen() Error!!!\n");
	   return 3;
	}

	// Open output file as write mode.
	if(testFileExists(outFile)>0)
	{
		puts("File Exists!");
		return 2;
	}
	out = fopen(outFile,"w");
	if(NULL == out)
	{
	   printf("\n fopen() Error!!!\n");
	   return 4;
	}


	// obtain file size:
	fseek (in , 0 , SEEK_END);
	lSize = ftell (in);
	rewind (in);

	int sufsize=KEYLEN_SHA1+GCRY_IV_SIZE+SALT_SIZE;
	fseek(in, -sufsize , SEEK_END);
	fread(salt,1,SALT_SIZE,in);
	fread(iv,1,GCRY_IV_SIZE,in);
	rewind (in);
	
	// Prepare cipher key and handle
	char * passwd = getPassword();
	char * key = genKey(passwd,salt);
	cipherPrep(handle,iv,key);
	hmacPrep(digest,key);
	
	int num = fread(buf,1,bufsize,in);
	int wrote=0,totalWrote=0;
	totalRead+=num;
	while(num!=0)
	{
		printf("read %d bytes, ",num);
		if(totalRead > lSize-sufsize)
			num = num-(totalRead-(lSize-sufsize));
		decrypt(outbuf,buf,num,key);	
		gcry_md_write(*digest,outbuf,num);
		wrote = fwrite(outbuf,1,num,out);
		totalWrote += wrote;
		printf("wrote %d bytes\n",wrote);
		if(totalRead > lSize-sufsize)
			break;

		num = fread(buf,1,bufsize,in);
		totalRead+=num;
	}

	// Read HMAC from last
	hmacBuf = gcry_md_read(*digest,GCRY_MD);

	fseek(in, -KEYLEN_SHA1 , SEEK_END);
	fread(hmacOrg,1,KEYLEN_SHA1,in);

	puts("Checking HMAC");

	if(memcmp(hmacBuf,hmacOrg,KEYLEN_SHA1)!=0)
	{
		perror("ERROR: DECRYPTION ERROR!");
		ret=1;
	}
	else
		puts("Decryption complete.");

	
	// Clost handles.
	fclose(out);
	fclose(in);
	gcry_cipher_close(*handle);
	gcry_md_close(*digest);

	// Free memory
	free(buf);
	free(outbuf);
	free(outFile);
	free(hmacOrg);
	free(iv);
	free(handle);
	free(digest);
	free(key);
	free(passwd);
	free(salt);

	if(ret==1)
		if(remove(outFile)!=0)
			perror("Remove corrupted file failed!.");
		else
			puts("Error file deleted.");

	return ret;
}

/*
	Start server, wait for connection and file transfer.
*/
int startServer(char* port)
{
	int bufsize=1024, totalRead=0;
	
	FILE * out;

    struct sockaddr_storage their_addr;
    socklen_t addr_size;
    struct addrinfo hints, *res;
    int sockfd, new_fd;



    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; 
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    getaddrinfo(NULL, port, &hints, &res);

    // make a socket, bind it, and listen on it:
    sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    bind(sockfd, res->ai_addr, res->ai_addrlen);
    listen(sockfd, 10);

    // now accept an incoming connection:
	while(1)
	{
    	addr_size = sizeof their_addr;
		puts("---- NETWORK ----");
		puts("Waiting for connections.");
	    new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &addr_size);
		puts("Connection established.");

		char* buf = (char*)malloc((sizeof(char))*(bufsize)); 
		char* outbuf=(char*)malloc((sizeof(char))*(bufsize)); 
		// use calloc to set all zero
		char* encFile=(char*)calloc(100,(sizeof(char))); 
		char* tmpFile=(char*)calloc(100,(sizeof(char))); 

		int lSize=1;
	
		// Recv: file size
		int bytes_recv = recv(new_fd, &lSize, sizeof(int) , 0);
		if(bytes_recv<0)
			printf("ERROR: %s\n",strerror(errno));
		printf("%d bytes file size.\n",bytes_recv);
	
		// Recv: file name
		int namesize=0;
		bytes_recv = recv(new_fd, &namesize, sizeof(int),0);
		bytes_recv = recv(new_fd, encFile, namesize,0);
		sprintf(tmpFile, "/tmp/%s", encFile); 	// temp file path
		printf("%d bytes file name.\n",bytes_recv);
	
	
		out = fopen(tmpFile,"w");
	
		// Recv: file content
		int totalRecv=0;
		while(totalRecv<lSize)
		{
			bytes_recv = recv(new_fd, buf, bufsize , 0);
			printf("Recv: %d\n",bytes_recv);
			totalRecv+=bytes_recv;
			if(bytes_recv==0)
				break;
			fwrite(buf,1, bytes_recv,out);
		}
		fclose(out);
	
		puts("Transfer complete.");
		// Send: result of decryption.
		int ret = localDec(tmpFile,1);	
		send(new_fd, &ret, sizeof(int),0);

		// free memory
		close(new_fd);
		free(buf);
		free(outbuf);
		free(encFile);
		free(tmpFile);

		puts("");

	}

}

/*
	Execute decryption steps.
*/
int decrypt(char* outbuf,char * buf,int size,char * key)
{
	/* CIPHER ENCRYPT */
	int gcryError = gcry_cipher_decrypt(*handle,outbuf, 1024, buf, size);
	if(gcryError)
	{
		printf("gcry_cipher_encrypt failed: %s/%s\n",gcry_strsource(gcryError),gcry_strerror(gcryError));
		return 1;
	}

	
}



