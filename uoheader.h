#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <gcrypt.h>
#include <errno.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <ctype.h> 

#define GCRY_CIPHER GCRY_CIPHER_AES256
#define GCRY_C_MODE GCRY_CIPHER_MODE_CFB
#define GCRY_MD GCRY_MD_SHA1
#define KEYLEN_SHA1 20
#define KEYLEN_MD5 16
#define GCRY_IV_SIZE 16 
#define SALT_SIZE 16

/*
	CFB, OFB and CTR mdoes do not requiere any special message operations.
*/

// Encryption
void options();
int encrypt(char*,char *,int size, char* key);
int localEnc(char * inputFile,char * outputFile);
int startClient(char*, char * sockstr);

// Decryption
int localDec(char * inputFile,int);
int decrypt(char*,char *,int size, char* key);
int startServer(char *);

// Common
char* getPassword();
int hmacPrep(gcry_md_hd_t* digest, char* key);
int cipherPrep(gcry_cipher_hd_t* handle, char* iv, char* key);
char* genKey(char* password,char* salt);
void getRndBuf(char*, int);

void error(const char *msg);
int testFileExists(char * fname);
void print_hex(unsigned char *buf, int len);

